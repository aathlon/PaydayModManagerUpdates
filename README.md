# Payday Mod Manager
Simple Payday mod manager that was originaly created for personal use. You can simply disable or enable back mods, including BLT, Mod Overrides or Maps. This tool also contains crash logs reader and interpreter (in development).

Mods are just moved to "disabledMods", "disabledOverrides" or "disabledMaps" folders which are being created in Payday 2 mod directory.

If you downloaded this program from the other sides than this GitLab repo or my website, I don't guarantee its safety. 
**YOU SHOULD ONLY DOWNLOAD IT FROM HERE!**
Also remember this tool is free - if you payed for it, you were scammed!

## Requirements
- Payday 2 (obviously)
- Windows Vista 32-bit or better
- .NET Framework 4.5

## Download
###### [Download](https://gitlab.com/aathlon/PaydayModManagerUpdates/raw/master/PaydayModManager.exe)

## Installation
Just download [Payday Mod Manager](https://gitlab.com/aathlon/PaydayModManagerUpdates/raw/master/PaydayModManager.exe) and paste it anywhere you wish. This tool doesn't require installation whatsoever.

## Author
- [Athlon](http://athlon.kkmr.pl/)